# Security Vision

Considere que uma empresa deseja aumentar sua segurança. Para isso, ela deseja utilizar um sistema computacional que analise imagens capturadas por câmeras, situadas em pontos estratégicos do estabelecimento, auxiliando o serviço dos seguranças locais em identificar se existem pessoas presentes no recinto.  

Para realizar essa tarefa, será utilizado o dataset [INRIA PERSON DATASET](http://pascal.inrialpes.fr/data/human/). Encontra-se na pasta positive, imagens de lugares com pessoas e na pasta negative, imagens de locais sem pessoas. [](url)

